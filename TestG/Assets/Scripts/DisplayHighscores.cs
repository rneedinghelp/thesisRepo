﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DisplayHighscores : MonoBehaviour {

    public Text score;

    string strScore;

    dreamloLeaderBoard dl;

    void Start()
    {
        

        for (int i = 0; i < score.text.Length; i++)
        {
            score.text = i + 1 + ". Fetching...";
        }


        dl = GetComponent<dreamloLeaderBoard>();
        StartCoroutine("RefreshHighscores");
    }

    void Update()
    {
        if (score == null || score.text == "")
        {
            score.text = "No internet connection\n detected,\n please connect to a\n Wi-Fi";
        }
        
    }

    public void DisplayLead()
    {
        //score.text = strScore;

        this.dl = dreamloLeaderBoard.GetSceneDreamloLeaderboard();

        List<dreamloLeaderBoard.Score> scoreList = dl.ToListHighToLow();


        if (scoreList == null)
        {
            //GUILayout.Label("(loading...)");
            score.text = "loading...";
        }
        else
        {
            score.text = "";
            int maxToDisplay = 20;
            int count = 0;
            foreach (dreamloLeaderBoard.Score currentScore in scoreList)
            {
                count++;
                score.text += currentScore.playerName.Replace('+',' ') + "\t\t" + currentScore.score + "\n";
                Debug.Log(currentScore);
                if (count >= maxToDisplay) break;
            }
        }
    }

    IEnumerator RefreshHighscores()
    {
        while (true)
        {
            this.dl = dreamloLeaderBoard.GetSceneDreamloLeaderboard();
            dl.LoadScores();
            yield return new WaitForSeconds(30);
        }
    }

        /*public Text[] highscoreFields;
        dreamloLeaderBoard highscoresManager;

        void Start() {
            for (int i = 0; i < highscoreFields.Length; i ++) {
                highscoreFields[i].text = i+1 + ". Fetching...";
            }


            highscoresManager = GetComponent<Highscores>();
            StartCoroutine("RefreshHighscores");
        }

        public void OnHighscoresDownloaded(Highscore[] highscoreList) {
            for (int i =0; i < highscoreFields.Length; i ++) {
                highscoreFields[i].text = i+1 + ". ";
                if (i < highscoreList.Length) {
                    highscoreFields[i].text += highscoreList[i].username + " - " + highscoreList[i].score;
                }
            }
        }

        IEnumerator RefreshHighscores() {
            while (true) {
                highscoresManager.DownloadHighscores();
                yield return new WaitForSeconds(30);
            }
        }*/
    }
