﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour {

    
    public Text questionText, questionTF, questionI;
    public Text txtChoice1, txtChoice2, txtChoice3, txtChoice4;
    public Text txtT, txtF, txtI;
    public GameObject cAud, wAud;
    public GameObject choicesQ, trueFalseQ, imageCorrect, imageWrong, idenQuiz;
    public GameObject hintPnl, pnlConfirm, pnlHint;
    public Text hint, coins;

    private int intCoins;
    private int qIndex, q2index, q3index;
    
    private int randomIndex;
    private static List<int> list = new List<int>();
    private bool hintUnlocked;

    private coinsGamesparks coinScr;
    private int lvl;

    // Use this for initialization
    void Start () {
        intCoins = PlayerPrefs.GetInt("coins", 10);

        coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();

        lvl = GameObject.Find("player").GetComponent<PlayerMovement>().currentLvl;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartDisplayIdenQuestion(QuestionPart2[] questions3)
    {
        questionI.text = questions3[q3index].question;
    }

    public void StartDisplayTFQuestion(QuestionPart2[] questions2)
    {

        //trueFalseQ.gameObject.SetActive(true);
        questionTF.text = questions2[q2index].question;
    }

    public void StartDisplayChoicesQuestion(Question[] questions)
    {
        //choicesQ.gameObject.SetActive(true);
        string[] txts = new string[4];
        string[] strings = new string[4];
        int randomIndex;

        strings[0] = questions[qIndex].ansC;
        strings[1] = questions[qIndex].ansW1;
        strings[2] = questions[qIndex].ansW2;
        strings[3] = questions[qIndex].ansW3;

        for (int n = 0; n < 4; n++)    //  Populate list
        {
            list.Add(n);
        }

        int iNum;

        for (int i = 0; i < 4; i++)
        {
            randomIndex = Random.Range(0, list.Count - 1);    //  Pick random element from the list
            iNum = list[randomIndex];    //  i = the number that was randomly picked
            list.RemoveAt(randomIndex);   //  Remove chosen element

            txts[iNum] = strings[i];

            //Buttons[i] = (int)Random.Range(1.0f,4.0f);
        }

        questionText.text = questions[qIndex].question;
        txtChoice1.text = txts[0];
        txtChoice2.text = txts[1];
        txtChoice3.text = txts[2];
        txtChoice4.text = txts[3];



        
    }

    SpriteRenderer rendererS;

    IEnumerator StartW()
    {
        Debug.Log("Wrong Coroutine popped up");
        imageWrong.SetActive(true);

        rendererS = imageWrong.GetComponent<SpriteRenderer>();

        /*rendererS.color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(2.5f);
        rendererS.color = new Color(1f, 1f, 1f, 0f);*/

        float start = Time.time;
        while (Time.time <= start + 2)
        {
            Color color = rendererS.color;
            color.a = 1f - Mathf.Clamp01((Time.time - start) / 2);
            rendererS.color = color;
            yield return new WaitForEndOfFrame();
        }

    }

    IEnumerator StartC()
    {
        imageCorrect.SetActive(true);

        rendererS = imageCorrect.GetComponent<SpriteRenderer>();

        /*
        imageCorrect.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(2.5f);
        imageCorrect.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);*/

        float start = Time.time;
        while (Time.time <= start + 2)
        {
            Color color = rendererS.color;
            color.a = 1f - Mathf.Clamp01((Time.time - start) / 2);
            rendererS.color = color;
            yield return new WaitForEndOfFrame();
        }

    }

    public void CheckIfRightChoice(Question[] questions, Text clickedT)
    {
        if (choicesQ.activeInHierarchy == true)
        {
            if (clickedT.text == questions[qIndex].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");

            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            qIndex++;
        }
        
        
        
        
    }

    public void OnClickTrueFalse(QuestionPart2[] questions2, Text clickedT)
    {
        if(trueFalseQ.activeInHierarchy == true)
        {
            if(clickedT.text == questions2[q2index].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");
            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            q2index++;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            if (clickedT.text == questions2[q3index].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");

            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            q3index++;
        }
    }

    public void DisplayHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        
        if (choicesQ.activeInHierarchy == true)
        {
            hint.text = questions[qIndex].hint;
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            hint.text = questions2[q2index].hint;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            hint.text = questions2[q3index].hint;
        }
        
    }

    public void CheckHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        
        if (choicesQ.activeInHierarchy == true)
        {
            questions[qIndex].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintA" + qIndex.ToString() + "lvl" + lvl.ToString(), "false"));
            hintUnlocked = questions[qIndex].hintUnlocked;
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            questions2[q2index].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintB" + q2index.ToString() + "lvl" + lvl.ToString(), "false"));
            hintUnlocked = questions2[q2index].hintUnlocked;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            questions3[q3index].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintC" + q3index.ToString() + "lvl" + lvl.ToString(), "false"));
            hintUnlocked = questions3[q3index].hintUnlocked;
        }

        if(hintUnlocked == false)
        {
            pnlConfirm.gameObject.SetActive(true);
        }
        else
        {
            pnlHint.gameObject.SetActive(true);
            DisplayHint(questions, questions2, questions3);
        }
    }

    public void UnlockHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        intCoins--;
        coins.text = intCoins.ToString();
        PlayerPrefs.SetInt("coins", intCoins);

        coinScr.SaveCoinsToGameSparks(intCoins);

        pnlHint.gameObject.SetActive(true);
        DisplayHint(questions, questions2, questions3);

        if (choicesQ.activeInHierarchy == true)
        {
            questions[qIndex].hintUnlocked = true;
            PlayerPrefs.SetString("hintA"+qIndex.ToString()+"lvl"+lvl.ToString(),"true");
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            questions2[q2index].hintUnlocked = true;
            PlayerPrefs.SetString("hintB" + q2index.ToString() + "lvl" + lvl.ToString(), "true");
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            questions3[q3index].hintUnlocked = true;
            PlayerPrefs.SetString("hintC" + q3index.ToString() + "lvl" + lvl.ToString(), "true");
        }
    }
}
