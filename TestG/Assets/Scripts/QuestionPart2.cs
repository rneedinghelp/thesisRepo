﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestionPart2 {

    [TextArea (3, 10)]
    public string question;

    public string ansC;

    [TextArea (1,1)]
    public string hint;

    public bool hintUnlocked;
    
}
