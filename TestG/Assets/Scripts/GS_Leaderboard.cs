﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GS_Leaderboard : MonoBehaviour
{
    //private PlayerMovement pmScript;
    
    private int lvl;
    private string highScStr;
    private float highSc;

    public string scoreEvent, scoreAttribute, lbName;
    public Text outputData;
    public GameObject highScorePopup, player;
    public bool isLevelScene;

    private string lbSocialN, scoreSocial;

    void Awake()
    {
        //GameSparks.Api.Messages.NewHighScoreMessage.Listener += HighScoreMessageHandler; // assign the New High Score message
        

        if (isLevelScene == false)
        {

            lvl = player.GetComponent<PlayerMovement>().currentLvl;
            highScStr = "HighScore" + player.GetComponent<PlayerMovement>().currentLvl.ToString();

            highSc = PlayerPrefs.GetFloat(highScStr, 0);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeNames(string lb)
    {
        lbName = lb;

        if(lbName == "LB_PTYPE")
        {
            scoreAttribute = "SCORE_ATR";
        }
    }

    public void ChangeSocialNames(string lbSocialName)
    {
        lbSocialN = lbSocialName;

        if(lbSocialN == "")
        {
            scoreSocial = "";
        }
    }

    public void PostScoreBttn()
    {
        
        Debug.Log("Posting Score To Leaderboard...");
        Debug.Log(highSc);
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey(scoreEvent)
            .SetEventAttribute(scoreAttribute, highSc.ToString())
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Score Posted Sucessfully...");
                }
                else
                {
                    Debug.Log("Error Posting Score...");
                }
            });
    }

    public void GetLeaderboard()
    {
        Debug.Log("Fetching Leaderboard Data...");

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {

            outputData.text = "You're not connected to the Internet. Please try again!";
        }
        else
        {
            new GameSparks.Api.Requests.LeaderboardDataRequest()
            .SetLeaderboardShortCode(lbName)
            .SetEntryCount(50) // we need to parse this text input, since the entry count only takes long
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Found Leaderboard Data...");
                    outputData.text = System.String.Empty; // first clear all the data from the output
                    foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                    {
                        int rank = (int)entry.Rank; // we can get the rank directly
                        string playerName = entry.UserName;
                        string score = entry.JSONData[scoreAttribute].ToString(); // we need to get the key, in order to get the score
                        outputData.text += rank + "\t\t\t" + playerName + "\t\t\t" + score + "\n"; // addd the score to the output text
                    }
                }
                else
                {
                    //Debug.Log("Error Retrieving Leaderboard Data...");
                    outputData.text = "Error Retrieving Leaderboard Data. Please try again!";
                }

            });
            Debug.Log(lbName);
        }
        
    }
	
	public void GetSocialLeaderboard(){

        Debug.Log("Fetching social leaderboard data");

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            outputData.text = "You're not connected to the Internet. Please try again!";
        }
        else
        {
            new GameSparks.Api.Requests.SocialLeaderboardDataRequest()
            .SetLeaderboardShortCode(lbName)
            .SetEntryCount(50) // we need to parse this text input, since the entry count only takes long
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Found Leaderboard Data...");
                    outputData.text = System.String.Empty; // first clear all the data from the output
                    foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                    {
                        int rank = (int)entry.Rank; // we can get the rank directly
                        string playerName = entry.UserName;
                        string score = entry.JSONData[scoreAttribute].ToString(); // we need to get the key, in order to get the score
                        outputData.text += rank + "\t\t\t" + playerName + "\t\t\t" + score + "\n"; // addd the score to the output text
                    }
                }
                else
                {
                    //Debug.Log("Social LB: Error Retrieving Leaderboard Data...");
                    outputData.text = "Error Retrieving Leaderboard Data. Please try again!";
                }

            });
        }

        
    }
}
