﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWinGamesparks : MonoBehaviour
{
    public GameObject gamesparkM, rewardImg;
    public Text victoryTxt;

    private coinsGamesparks coinScr;
    private PlayerMovement playM;
    private bool isPerfect;

    // Start is called before the first frame update
    void Start()
    {
        if(this.enabled == true)
        {
            gamesparkM.GetComponent<GS_Leaderboard>().PostScoreBttn();

            coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();
            playM = GameObject.Find("player").GetComponent<PlayerMovement>();

            isPerfect = playM.isPerfect;

            if(isPerfect == true)
            {
                //shows an ui text that the player have completed with a perfect score
                //then rewards the player coins 

                victoryTxt.text = "Perfect Score!";

                //set the ui that rewards the player active
                rewardImg.gameObject.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickReward(int rewardCoins)
    {
        int coins = PlayerPrefs.GetInt("coins", 10);
        coinScr.SaveCoinsToGameSparks(coins + rewardCoins);
        PlayerPrefs.SetInt("coins", coins + rewardCoins);
    }
}
