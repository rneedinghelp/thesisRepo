﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Requests;



public class coinsScript : MonoBehaviour
{
    public Text coins;
    private int pCoins, played;
    private coinsGamesparks coinScr;
    

    // Start is called before the first frame update
    void Start()
    {
        coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();
        

        played = PlayerPrefs.GetInt("played", 0);
        
        
        pCoins = PlayerPrefs.GetInt("coins",10);
        coins.text = pCoins.ToString();

        coinScr.SaveCoinsToGameSparks(pCoins);
        //LoadCoinsFromGameSparks();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (played == 0)
        {
            Debug.Log(played);
            
            new AccountDetailsRequest().Send((response) =>
            {
                if (response.Currency1 == 0)
                {
                    AddCurrency(1, 10);
                }
                pCoins = (int)response.Currency1;
                Debug.Log(pCoins);

            });
            PlayerPrefs.SetInt("played", 1);
            PlayerPrefs.SetInt("coins", pCoins);
        }*/
        //SaveCoinsToGameSparks(PlayerPrefs.GetInt("coins",10));
            
        
    }

    public void AddCurrency(int currencyRef, int amount)
    {

        Debug.Log("Calling AddCurrency ... ");

        new LogEventRequest().SetEventKey("AddCurrency").SetEventAttribute("currencyRef", currencyRef).SetEventAttribute("amount", amount).Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log("Log message failed ...");
            }
            else
            {
                Debug.Log("Log message succeeded ..." + response);
            }
        }
        );

        
    }

    
}
